package com.example.gallowsmultiplayer;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

public class GameOverActivity extends AppCompatActivity {

    TextView textViewGameOver;
    Intent intent;
    String text="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_over);

        textViewGameOver=findViewById(R.id.textViewGameOver);

        intent = getIntent();
        text = intent.getStringExtra("WonPlayerName");

        textViewGameOver.setText(text + " has won!");
    }
}