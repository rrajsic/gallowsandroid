package com.example.gallowsmultiplayer;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

    private static RetrofitClient instance = null;
    private WordsAPI wordAPI;

    private RetrofitClient() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(WordsAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        wordAPI = retrofit.create(WordsAPI.class);
    }

    public static synchronized RetrofitClient getInstance() {
        if (instance == null) {
            instance = new RetrofitClient();
        }
        return instance;
    }

    public WordsAPI getMyApi() {
        return wordAPI;
    }
}
