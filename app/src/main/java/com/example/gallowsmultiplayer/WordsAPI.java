package com.example.gallowsmultiplayer;
import java.util.List;

import javax.xml.transform.Result;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface WordsAPI {

    String BASE_URL = "https://random-words-api.vercel.app/";

    @GET("word")
    Call<List<Word>> getWord();

}