package com.example.gallowsmultiplayer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class RoomSearchActivity extends AppCompatActivity {

    ListView listView;
    Button button;

    List<String> roomList;

    String roomName="";
    String playerName="";

    FirebaseDatabase database;
    DatabaseReference roomRef;
    DatabaseReference roomsRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room_search);

        database = FirebaseDatabase.getInstance("https://gallows2-cdfb3-default-rtdb.firebaseio.com/");

        //get the player name and assign it to room name
        SharedPreferences preferences = getSharedPreferences("PREFS", 0);
        playerName = preferences.getString("playerName", "");
        roomName = playerName;

        listView = findViewById(R.id.listView);
        button = findViewById(R.id.buttonCreateRoom);

        //all available rooms
        roomList = new ArrayList<>();

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //create room and add yourself as player1
                button.setText("CREATING ROOM");
                button.setEnabled(false);
                roomName = playerName;
                roomRef = database.getReference("rooms/" + roomName + "/player1");
                addRoomEventListener();
                roomRef.setValue(playerName);
            }
        });

        listView.setOnItemClickListener((parent, view, position, id) -> {
            //join an existing room
            roomName = roomList.get(position);
            roomRef = database.getReference("rooms/" + roomName + "/player2");
            addRoomEventListener();
            roomRef.setValue(playerName);
        });
        //show if new room is available
        addRoomsEventListener();

    }
        private void addRoomEventListener() {
            roomRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    //join the room
                    button.setText("CREATE ROOM");
                    button.setEnabled(true);
                    Intent intent = new Intent(getApplicationContext(), RoomActivity.class);
                    intent.putExtra("roomName", roomName);
                    startActivity(intent);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //error
                    button.setText("CREATE ROOM");
                    button.setEnabled(true);
                    Toast.makeText(RoomSearchActivity.this, "Error", Toast.LENGTH_SHORT).show();
                }
            });
        }
        private void addRoomsEventListener(){
            roomsRef = database.getReference("rooms");
            roomsRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    //show list of rooms
                    roomList.clear();
                    Iterable<DataSnapshot> rooms = snapshot.getChildren();
                    for(DataSnapshot snap : rooms){
                        roomList.add(snap.getKey());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(RoomSearchActivity.this,
                           R.layout.activity_listview,R.id.roomTextView,roomList);
                    listView.setAdapter(adapter);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    //error - nothing
                }
            });
        }
    }
