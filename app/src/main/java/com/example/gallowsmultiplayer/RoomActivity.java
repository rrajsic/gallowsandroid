package com.example.gallowsmultiplayer;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoomActivity extends AppCompatActivity {

    Button buttonPoke;
    TextView definitionText;


    Word wordObject_g = new Word();

    String playerName="";
    String roomName="";
    String role ="";
    String word="";
    String definition="";
    String player1Name="";
    String player2Name="";

    Context context;
    CardView cardview;
    LinearLayout.LayoutParams layoutparams;
    TextView textview;
    LinearLayout linearLayout;

    TextView player1missed;
    TextView player2missed;

    EditText guessLetter;

    FirebaseDatabase database;
    DatabaseReference roomRef;
    DatabaseReference player1Ref;
    DatabaseReference player2Ref;
    DatabaseReference wordRef;
    DatabaseReference definitionRef;
    DatabaseReference player1GuessRef;
    DatabaseReference player2GuessRef;
    DatabaseReference player1GuessCountRef;
    DatabaseReference player2GuessCountRef;
    DatabaseReference guessLettersRef;

    int p1CorrectGuessCount = 0;
    int p2CorrectGuessCount =0;
    int p1CorrectGuessCount_g=0;
    int p2CorrectGuessCount_g=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);


        buttonPoke = findViewById(R.id.buttonPoke);

        definitionText = findViewById(R.id.definitionText);

        player1missed = findViewById(R.id.player1missed);
        player2missed = findViewById(R.id.player2missed);
        guessLetter = findViewById(R.id.guessLetter);
        context = getApplicationContext();

        linearLayout = findViewById(R.id.linearLayout);

        database = FirebaseDatabase.getInstance("https://gallows2-cdfb3-default-rtdb.firebaseio.com/");

        SharedPreferences preferences = getSharedPreferences("PREFS", 0);
        playerName = preferences.getString("playerName", "");

        Bundle extras = getIntent().getExtras();

        if(extras !=null){
            roomName = extras.getString("roomName");
            if(roomName.equals(playerName)){
                role = "host";
            } else{
                role = "guest";
            }
        }

        roomRef=database.getReference("rooms/"+roomName);
        player1Ref=database.getReference("rooms/"+roomName+"/player1");
        player2Ref=database.getReference("rooms/"+roomName+"/player2");

        roomRef.onDisconnect().removeValue();
        player1Ref.onDisconnect().removeValue();
        player2Ref.onDisconnect().removeValue();

        player1GuessRef=database.getReference("rooms/"+roomName+"/player1-guesses");
        player2GuessRef=database.getReference("rooms/"+roomName+"/player2-guesses");

        player1GuessCountRef=database.getReference("rooms/"+roomName+"/player1-guess-count");
        player2GuessCountRef=database.getReference("rooms/"+roomName+"/player2-guess-count");

        guessLettersRef=database.getReference("rooms/"+roomName+"/guessed-letters");

        if(!roomName.equals(playerName)){
            buttonPoke.setEnabled(false);
        }

        buttonPoke.setOnClickListener(v -> {
            if(word!=null) {
                String guess = guessLetter.getText().toString().toLowerCase();
                if (word.contains(guess.toLowerCase())) {
                    if (roomName.equals(playerName)) {
                        player1GuessRef.child(guess).setValue(Boolean.toString(true));
                        guessLettersRef.child(guess).setValue(playerName);
                        Toast.makeText(RoomActivity.this,"Correct",Toast.LENGTH_SHORT).show();
                        p1CorrectGuessCount++;
                        player1GuessCountRef.setValue(Integer.toString(p1CorrectGuessCount));

                    } else {
                        player2GuessRef.child(guess).setValue(Boolean.toString(true));
                        guessLettersRef.child(guess).setValue(playerName);
                        Toast.makeText(RoomActivity.this,"Correct",Toast.LENGTH_SHORT).show();
                        p2CorrectGuessCount++;
                        player2GuessCountRef.setValue(Integer.toString(p2CorrectGuessCount));
                    }
                } else {
                    if (roomName.equals(playerName)) {
                        player1GuessRef.child(guess).setValue(Boolean.toString(false));
                        Toast.makeText(RoomActivity.this,"Miss",Toast.LENGTH_SHORT).show();
                        buttonPoke.setEnabled(false);


                    } else {
                        player2GuessRef.child(guess).setValue(Boolean.toString(false));
                        Toast.makeText(RoomActivity.this,"Miss",Toast.LENGTH_SHORT).show();
                        buttonPoke.setEnabled(false);
                    }
                }
            }
        });


        wordRef = database.getReference("rooms/" + roomName+"/"+"word");
        definitionRef=database.getReference("rooms/"+ roomName+"/"+"definition");

        if(role=="host")startGallows();
        else{
            addWordEventListener();
            addDefinitionEventListener();
        }
        addPlayer1EventListener();
        addPlayer2EventListener();

        addPlayer1MissedEventListener();
        addPlayer2MissedEventListener();

        addGuessedLetterEventListener();
        addGuessedLetterCountEventListener();
        addPlayer1GuessCountEventListener();
        addPlayer2GuessCountEventListener();

    }
    private void addPlayer1GuessCountEventListener(){
        player1GuessCountRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(snapshot.getValue(String.class)!=null)
                p1CorrectGuessCount_g = Integer.valueOf(snapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }
    private void addPlayer2GuessCountEventListener(){
        player2GuessCountRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(snapshot.getValue(String.class)!=null)
                p2CorrectGuessCount_g = Integer.valueOf(snapshot.getValue(String.class));
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }

    private void addGuessedLetterCountEventListener(){
        guessLettersRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(word!=null && !word.equals("")){

                    if(snapshot.getChildrenCount() == (numberOfUniqueCharacters(word))){
                        String player_won="";
                        for(DataSnapshot temp : snapshot.getChildren()){
                            player_won = temp.getValue(String.class);
                        }

                        Intent intent = new Intent(getApplicationContext(), GameOverActivity.class);
                        intent.putExtra("WonPlayerName", player_won);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {

            }
        });
    }
    private void addGuessedLetterEventListener(){
        guessLettersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {
                if(snapshot.getValue(String.class)!=null){
                    if(word !=null){
                        String guess = snapshot.getKey();
                        writeCorrectGuess(guess.charAt(0));
                    }
                }
            }

            @Override
            public void onChildChanged(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {

            }

            @Override
            public void onChildRemoved(@NonNull @NotNull DataSnapshot snapshot) {

            }

            @Override
            public void onChildMoved(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {

            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
    private void addPlayer1MissedEventListener(){
        player1GuessRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull @NotNull DataSnapshot snapshot,String prevChildKey) {
                if(snapshot.getValue(String.class).equals("false")) {
                    if (!(roomName.equals(playerName)))
                        buttonPoke.setEnabled(true);


                    player1missed.append(snapshot.getKey().toUpperCase() + " ");
                }
            }

            @Override
            public void onChildChanged(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {
            }

            @Override
            public void onChildRemoved(@NonNull @NotNull DataSnapshot snapshot) {
            }

            @Override
            public void onChildMoved(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
    private void addPlayer2MissedEventListener(){
        player2GuessRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(@NonNull @NotNull DataSnapshot snapshot,String prevChildKey) {
                if(snapshot.getValue(String.class).equals("false")) {
                    if (roomName.equals(playerName))
                        buttonPoke.setEnabled(true);

                    player2missed.append(snapshot.getKey().toUpperCase() +" ");
                }
            }

            @Override
            public void onChildChanged(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {
            }

            @Override
            public void onChildRemoved(@NonNull @NotNull DataSnapshot snapshot) {
            }

            @Override
            public void onChildMoved(@NonNull @NotNull DataSnapshot snapshot, @Nullable @org.jetbrains.annotations.Nullable String previousChildName) {
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }

    private void addPlayer1EventListener(){
        player1Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                player1missed.append(snapshot.getValue(String.class)+": ");
                player1Name = snapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
    private void addPlayer2EventListener(){
        player2Ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull @NotNull DataSnapshot snapshot) {
                if(!(snapshot.getValue(String.class)==null))
                player2missed.append(snapshot.getValue(String.class)+": ");
                player2Name = snapshot.getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull @NotNull DatabaseError error) {
            }
        });
    }
    private void addWordEventListener(){
        wordRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //message revieved
                if (!(snapshot.getValue(String.class) == null)) {
                    wordObject_g.setWord(snapshot.getValue(String.class));
                    word = wordObject_g.getWord().toLowerCase();
                    for(int i =0; i<word.length();i++){
                        createCardView(new Character(word.charAt(i)).toString());
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }
    private void addDefinitionEventListener(){
        definitionRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                //message revieved
                if(!(snapshot.getValue(String.class) == null)){
                wordObject_g.setDefinition(snapshot.getValue(String.class));
                definitionText.setText(wordObject_g.getDefinition());
              }
                else {
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
            }
        });
    }

    private void createCardView(String l){
        cardview = new CardView(context);

        layoutparams = new LinearLayout.LayoutParams(
                125,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutparams.setMargins(10,0,15,25);
        cardview.setLayoutParams(layoutparams);
        cardview.setRadius(15);
        cardview.setPadding(25, 25, 25, 25);
        cardview.setCardBackgroundColor(Color.BLUE);
        cardview.setMinimumWidth(100);
        textview = new TextView(context);
        textview.setLayoutParams(layoutparams);
        textview.setText(l.toLowerCase());
        textview.setVisibility(View.INVISIBLE);
        textview.setTextSize(TypedValue.COMPLEX_UNIT_DIP, 25);
        textview.setTextColor(Color.WHITE);
        textview.setPadding(25,25,25,25);
        textview.setGravity(Gravity.CENTER);

        cardview.addView(textview);
        linearLayout.addView(cardview);
    }

    private void startGallows(){
        Call<List<Word>> call = RetrofitClient.getInstance().getMyApi().getWord();

        call.enqueue(new Callback<List<Word>>() {
            @Override
            public void onResponse(Call<List<Word>> call, Response<List<Word>> response) {

                if (response.isSuccessful()) {
                    Word wordobj = response.body().get(0);

                    Toast.makeText(RoomActivity.this,wordobj.getWord(),Toast.LENGTH_LONG).show();

                    word = wordobj.getWord();
                    definition=wordobj.getDefinition();


                    wordRef.setValue(word.toLowerCase());
                    definitionRef.setValue(definition);

                    addWordEventListener();
                    addDefinitionEventListener();

                } else {
                    return;
                }
            }

            @Override
            public void onFailure(Call<List<Word>> call, Throwable t) {
            }
        });
    }

    private void writeCorrectGuess(char guess){
        for(int i =0;i<word.length();i++){
            if(word.charAt(i) == guess){
                CardView card_temp;
                card_temp = (CardView) linearLayout.getChildAt(i);
                TextView text_temp;
                text_temp = (TextView) card_temp.getChildAt(0);

               text_temp.setVisibility(View.VISIBLE);
            }
        }
    }
    private int numberOfUniqueCharacters(String word){
        int counter=0;
        String temp=" ";
        boolean flag;
        for(int i =0 ;i < word.length();i++){
            flag=false;
            for(int j =0;j<temp.length();j++) {
                if (word.charAt(i) == temp.charAt(j)) {
                    flag = true;
                    break;
                }
            }
                if(!flag){
                    counter++;
                    temp+=word.charAt(i);
                    Log.d("WETEFE", "counter"+counter);
                    Log.d("WETEFE", "TEMP: "+temp);
                }
        }
        return counter;
    }
}